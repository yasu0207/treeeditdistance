# Tree Edit Distance
Integer Linear Programming formulations for the unordered tree edit distance problem described in papers [1, 2]. To run the code, it requires Java 1.8 (or higher) and CPLEX.

[1] S. Kondo, K. Otaki, M. Ikeda, A. Yamamoto: Fast Computation of the Tree Edit Distance between Unordered Trees Using IP Solvers. In Proc. of DS 2014, vol. 8777 of LNCS, pp. 156-167 (2014)

[2] E. Hong, Y. Kobayashi, A. Yamamoto: Improved Methods for Computing Distances Between Unordered Trees Using Integer Programming. In Proc. of COCOA 2017, vol. 10628 of LNCS, pp. 45-60 (2017)
