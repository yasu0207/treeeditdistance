import java.util.Iterator;
import java.util.ArrayList;

public class Node implements Iterable< Node > {
	
	public Node parent;
	public int id;
	public int size;    // the number of descendants (including itself)
	public int height;   // the height of the node (the height of leaf = 1)
	
	public String label;
	
	ArrayList<Node> children;
	
	public Node(int id, String label)
	{
		this.id = id;
		this.label = label;
		children = new ArrayList<>();
	}
	
	public void addChild(Node child)
	{
		children.add(child);
		child.parent = this;
	}
	
	// returns true if this node is a leaf
	public boolean isLeaf()
	{
		return children.isEmpty();
	}
	
	// returns the number of children
	public int children()
	{
		return children.size();
	}

	@Override
	public Iterator< Node > iterator()
	{
		return children.iterator();
	}
	
	@Override
	public String toString()
	{
		return "[" + id + " " + label + "]";
	}
	
	
	public void printSubtree()
	{
		printSubtree( 0 );
	}
	
	private void printSubtree(int r)
	{
		for (int i = 0; i < r; i++) {
			System.out.print( "-" );
		}
		System.out.println( this );
		for (Node c: children) {
			c.printSubtree( r + 1 );
		}
	}
}
