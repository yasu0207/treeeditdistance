import ilog.concert.IloException;
import ilog.concert.IloNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import javax.jws.WebParam.Mode;

public class DpIP_Bot { // Bottom-up by DP (improved)
	
	private static final int REPLACE_COST = 1;
	private static final int DELETE_COST = 1;
	private static final int INSERT_COST = 1;

	private int[][] dp;
	private boolean[][] isomorphic;
	private ArrayList<Node> t1;
	private ArrayList<Node> t2;
	
	private ArrayList<Node[]>[][] submapping;
	private ArrayList<Node[]> mapping;

	public DpIP_Bot(Node r1, Node r2)
	{
		t1 = new ArrayList<>();
		t2 = new ArrayList<>();
		dfs(r1, t1);
		dfs(r2, t2);
	}

	public int solve()
	{
		int W1 = 0;
		for (Node x: t1) {
			W1 += weight(x.label, null);
		}
		int W2 = 0;
		for (Node y: t2) {
			W2 += weight(null, y.label);
		}

		dp = new int[t1.size()][t2.size()];
		isomorphic = new boolean[t1.size()][t2.size()];
		submapping = new ArrayList[t1.size()][t2.size()];
		Collections.sort(t1, (x, y) -> Integer.compare(x.height, y.height));
		Collections.sort(t2, (x, y) -> Integer.compare(x.height, y.height));
		for (Node x: t1) {
			for (Node y: t2) {
				isomorphic[x.id][y.id] = isIsomorphic(x, y);
				dp[x.id][y.id] = run(x, y);
			}
		}
		
		int opt = -1;
		IloCplex model = null;
		try {
			model = new IloCplex();
			model.setOut( null ); // avoid cplex's output
			model.setParam(IloCplex.IntParam.Threads, 1); // single thread

			IloNumVar[][] m = new IloNumVar[t1.size()][t2.size()];
			for (Node x: t1) {
				for (Node y: t2) {
					m[x.id][y.id] = model.boolVar();
				}
			}

			IloNumExpr obj = null;
			for (Node x: t1) {
				for (Node y: t2) {
					obj = sum(model, obj, model.prod(dp[x.id][y.id], m[x.id][y.id]));
				}
			}

			model.addMaximize( obj );

			// add constraints: for each path P between the root and a leaf, m(P) must be at most one
			for (Node x: t1) {
				if (x.isLeaf() == false) continue;
				// x must be a leaf
				IloNumExpr constraint = null;
				while (x != null) {
					for (Node y: t2) {
						constraint = sum(model, constraint, m[x.id][y.id]);
					}
					x = x.parent;
				}

				if (constraint != null) {
					model.addLe(constraint, 1);
				}

			}
			for (Node y: t2) {
				if (y.isLeaf() == false) continue;
				// y must be a leaf
				IloNumExpr constraint = null;
				while (y != null) {
					for (Node x: t1) {
						constraint = sum(model, constraint, m[x.id][y.id]);
					}
					y = y.parent;
				}

				if (constraint != null) {
					model.addLe(constraint, 1);
				}
			}

			if (model.solve()) {
				opt = W1 + W2 - (int)(model.getObjValue() + 0.5);
				
				mapping = new ArrayList<>();
				
				for (Node x: t1) {
					for (Node y: t2) {
						if (model.getValue(m[x.id][y.id]) > 0.5) {
							collectMapping(x, y);
						}
					}
				}
			}
		} catch (IloException e) {
			e.printStackTrace();
		} finally {
			if (model != null) {
				model.end();
			}
		}
		return opt;
	}

	// returns true if trees rooted at r1 and at r2 are structurally isomorphic (i.e. isomorphic ignoring labels)
	private boolean isIsomorphic(Node r1, Node r2)
	{
		if (r1.size != r2.size) return false;
		if (r1.height != r2.height) return false;
		if (r1.children() != r2.children()) return false;
		int N = r1.children();
		Node[] c1 = new Node[ N ]; // the children of r1
		Node[] c2 = new Node[ N ]; // the children of r2
		int k = 0;
		for (Node c: r1) {
			c1[ k++ ] = c;
		}
		k = 0;
		for (Node c: r2) {
			c2[ k++ ] = c;
		}
		boolean[] match = new boolean[ N ];
		L: for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				if (match[ j ] == false && isomorphic[ c1[ i ].id ][ c2[ j ].id ]) {
					match[ j ] = true;
					continue L;
				}
			}
			return false;
		}
		return true;
	}

	// Assumption: the weight of a maximum weight mapping between every pair of vertices in the subtree rooted at r1 and vertices in that at r2.
	private int run(Node r1, Node r2)
	{
		if (r1.isLeaf() && r2.isLeaf()) {
			return mappingWeight(r1.label, r2.label);
		}
		if (r1.isLeaf() || r2.isLeaf()) {
			return 0;
		}
		if (isomorphic[r1.id][r2.id] == false) {
			return 0;
		}
		int n = r1.children();
		MinCostMatching mcm = new MinCostMatching(n, n);
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				mcm.add(i, j, -dp[r1.children.get(i).id][r2.children.get(j).id]);
			}
		}
		
		int result = mcm.get();
		submapping[r1.id][r2.id] = new ArrayList<>();
		for (int[] pair: mcm.getMatching()) {
			submapping[r1.id][r2.id].add(new Node[] {r1.children.get(pair[0]), r2.children.get(pair[1])});
		}
		return -result + mappingWeight(r1.label, r2.label);
	}
	
	private void collectMapping(Node x, Node y)
	{
		mapping.add(new Node[] {x, y});
		if (submapping[x.id][y.id] != null) {
			for (Node[] sub: submapping[x.id][y.id]) {
				collectMapping(sub[0], sub[1]);
			}
		}
	}

	public ArrayList<Node[]> getMapping()
	{
		if (mapping == null) {
			solve();
		}
		return mapping;
	}
	
	private IloNumExpr sum(IloCplex model, IloNumExpr ex1, IloNumExpr ex2) throws IloException {
		if (ex1 == null) return ex2;
		if (ex2 == null) return ex1;
		return model.sum(ex1, ex2);
	}

	private int weight(String label1, String label2)
	{
		if (label1 == null) return DELETE_COST;
		if (label2 == null) return INSERT_COST;
		if (label1.equals(label2)) return 0;
		return REPLACE_COST;
	}
	
	private int mappingWeight(String label1, String label2)
	{
		return DELETE_COST + INSERT_COST - weight(label1, label2);
	}

	private void dfs(Node r, ArrayList<Node> list) 
	{
		list.add(r);
		r.height = 0;
		r.size = 1;
		for (Node ch: r) {
			dfs(ch, list);
			r.height = Math.max(r.height, ch.height + 1);
			r.size += ch.size;
		}
	}
}
