import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class KCFReader {
	
	public static Node read(String fileName)
	{
		try {
			Scanner sc = new Scanner(new File(fileName));
			String treeData = "";
			
			while ((sc.next()).equals("NODE") == false);
			
			int N = sc.nextInt();
			Node[] nodes = new Node[N];
			for (int i = 0; i < N; i++) {
				sc.next();
				String label = sc.next();
				nodes[i] = new Node(i, label);
				sc.next();
				sc.next();
			}
			
			while ((sc.next()).equals("EDGE") == false);
			
			int M = sc.nextInt();
			ArrayList<Integer>[] edges = new ArrayList[N];
			for (int i = 0; i < N; i++) {
				edges[i] = new ArrayList<>();
			}
			for (int i = 0; i < M; i++) {
				sc.nextInt();
				int s = Integer.parseInt(sc.next().split(":")[0]) - 1;
				int t = Integer.parseInt(sc.next().split(":")[0]) - 1;
				edges[s].add(t);
				edges[t].add(s);
			}
			dfs(0, -1, nodes, edges);
			return nodes[0];
		} catch (FileNotFoundException e) {
			return null;
		}
	}

	private static void dfs(int u, int p, Node[] nodes, ArrayList<Integer>[] edges) {
		for (int v: edges[u]) {
			if (v != p) {
				nodes[u].addChild(nodes[v]);
				dfs(v, u, nodes, edges);
			}
		}
	}
	
	public static void main(String[] args) {
		Node root = KCFReader.read("G01000.kcf");
		root.printSubtree();
	}

}
