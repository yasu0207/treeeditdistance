import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class CSLogReader {
	
	ArrayList<Node> trees;
	int[] size;
	int[] degree;
	
	public CSLogReader(String fileName) throws FileNotFoundException
	{
		trees = new ArrayList<Node>();
		
		Scanner scan = new Scanner(new File(fileName));
		while(scan.hasNextLine()) {
			String line = scan.nextLine();
			String[] tokens = line.split("\\s+");
			Node root = new Node(0, tokens[2]);
			trees.add(root);
			Node node = root;
			for (int i = 3, id = 1; i < tokens.length; i++) {
				if (tokens[i].equals("-1")) {
					node = node.parent;
				} else {
					Node child = new Node(id++, tokens[i]);
					node.addChild(child);
					node = child;
				}
			}
		}
		size = new int[trees.size()];
		degree = new int[trees.size()];
		for (int i = 0; i < trees.size(); i++) {
			Node root = trees.get(i);
			dfs(root, i);
		}
	}
	
	public ArrayList<Node> getTrees(int lsize, int usize, int ldegree, int udegree)
	{
		ArrayList<Node> res = new ArrayList<>();
		for (int i = 0; i < trees.size(); i++) {
			if (lsize <= size[i] && size[i] <= usize && ldegree <= degree[i] && degree[i] <= udegree) {
				res.add(trees.get(i));
			}
		}
		
		return res;
	}

	private void dfs(Node node, int id)
	{
		size[id]++;
		degree[id] = Math.max(degree[id], node.children());
		for (Node ch: node) {
			dfs(ch, id);
		}
	}
}
