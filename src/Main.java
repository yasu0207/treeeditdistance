import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Random;

public class Main {
	
	public static Node randomTree(int N, int domain)
	{
		long seed = new Random().nextLong();
		System.out.println(seed);
		return randomTree(N, seed, domain);
	}
	public static Node randomTree(int N, long seed, int domain)
	{
		Random rnd = new Random( seed );
		Node[] nodes = new Node[N];
		for (int i = 0; i < N; i++) {
			String label = Integer.toString(rnd.nextInt(domain));
			nodes[i] = new Node(i, label);
		}
		
		for (int i = 1; i < N; i++) {
			int parent = rnd.nextInt( i );
			nodes[parent].addChild(nodes[i]);
		}
		
		return nodes[0];
	}
	
	public static void main(String[] args) throws FileNotFoundException {
		
		Node r1 = randomTree(40, 2);
		Node r2 = randomTree(40, 2);
		IP_Bot ip = new IP_Bot(r1, r2);
		long start = System.currentTimeMillis();
		int ipv = ip.solve();
		long end = System.currentTimeMillis();
		System.out.println(r1.size + " " + r2.size + " " + ipv + " " + (end - start));
		
		System.out.println("== mapping ==");
		for (Node[] pair: ip.getMapping()) {
			System.out.println(pair[0].id + " " + pair[1].id);
		}
		
		System.out.println("==");
		DpIP_Bot dp = new DpIP_Bot(r1, r2);
		start = System.currentTimeMillis();
		int dpv = dp.solve();
		end = System.currentTimeMillis();
		System.out.println(r1.size + " " + r2.size + " " + dpv + " " + (end - start));
		System.out.println("== mapping ==");
		for (Node[] pair: dp.getMapping()) {
			System.out.println(pair[0].id + " " + pair[1].id);
		}
	}
}
