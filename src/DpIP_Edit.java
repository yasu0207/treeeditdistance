import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import ilog.concert.IloException;
import ilog.concert.IloNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;

// compute the TED by the IP formulation due to [Hong et al., 2017]
public class DpIP_Edit {
	
	private static final int REPLACE_COST = 1;
	private static final int DELETE_COST = 1;
	private static final int INSERT_COST = 1;

	private int[][] dp;
	
	private ArrayList<Node> t1;
	private ArrayList<Node> t2;
	
	private ArrayList<Node[]>[][] submapping;
	private ArrayList<Node[]> mapping;
	
	public DpIP_Edit(Node r1, Node r2)
	{
		t1 = new ArrayList<>();
		t2 = new ArrayList<>();
		dfs(r1, t1, true);
		dfs(r2, t2, true);
	}

	public int solve()
	{
		int W1 = 0;
		for (Node x: t1) {
			W1 += weight(x.label, null);
		}
		int W2 = 0;
		for (Node y: t2) {
			W2 += weight(null, y.label);
		}

		dp = new int[t1.size()][t2.size()];
		submapping = new ArrayList[t1.size()][t2.size()];
		Collections.sort(t1, (x, y) -> Integer.compare(x.height, y.height));
		Collections.sort(t2, (x, y) -> Integer.compare(x.height, y.height));
		for (Node x: t1) {
			for (Node y: t2) {
				dp[x.id][y.id] = run(x, y);
			}
		}
		
		IloCplex model = null;
		int opt = -1;
		try {
			model = new IloCplex();
			model.setOut( null ); // avoid cplex's output
			model.setParam(IloCplex.IntParam.Threads, 1); // single thread

			IloNumVar[][] m = new IloNumVar[t1.size()][t2.size()];
			for (Node x: t1) {
				for (Node y: t2) {
					m[x.id][y.id] = model.boolVar();
				}
			}

			IloNumExpr obj = null;
			for (Node x: t1) {
				for (Node y: t2) {
					obj = sum(model, obj, model.prod(dp[x.id][y.id], m[x.id][y.id]));
				}
			}

			model.addMaximize( obj );

			// add constraints: for each path P between the root and a leaf, m(P) must be at most one
			for (Node x: t1) {
				if (x.isLeaf() == false) continue;
				// x must be a leaf
				IloNumExpr constraint = null;
				while (x != null) {
					for (Node y: t2) {
						constraint = sum(model, constraint, m[x.id][y.id]);
					}
					x = x.parent;
				}

				if (constraint != null) {
					model.addLe(constraint, 1);
				}

			}
			for (Node y: t2) {
				if (y.isLeaf() == false) continue;
				// y must be a leaf
				IloNumExpr constraint = null;
				while (y != null) {
					for (Node x: t1) {
						constraint = sum(model, constraint, m[x.id][y.id]);
					}
					y = y.parent;
				}

				if (constraint != null) {
					model.addLe(constraint, 1);
				}
			}

			if (model.solve()) {
				opt = W1 + W2 - (int)(model.getObjValue() + 0.5);
				
				mapping = new ArrayList<>();
				
				for (Node x: t1) {
					for (Node y: t2) {
						if (model.getValue(m[x.id][y.id]) > 0.5) {
							collectMapping(x, y);
						}
					}
				}
			}
		} catch (IloException e) {
			e.printStackTrace();
		} finally {
			if (model != null) {
				model.end();
			}
		}
		return opt;
	}
	
	
	
	private void collectMapping(Node x, Node y)
	{
		mapping.add(new Node[] {x, y});
		if (submapping[x.id][y.id] != null) {
			for (Node[] sub: submapping[x.id][y.id]) {
				collectMapping(sub[0], sub[1]);
			}
		}
	}

	public ArrayList<Node[]> getMapping()
	{
		if (mapping == null) {
			solve();
		}
		return mapping;
	}

	// computes the maximum weight of Tai mapping between the subtrees rooted at x and at y that contains the root pair (x, y).
	private int run(Node x, Node y)
	{
		if (x.isLeaf() || y.isLeaf()) {
			return weight(x.label, null) + weight(null, y.label) - weight(x.label, y.label);
		}
		
		ArrayList<Node> subtree1 = new ArrayList<>();
		ArrayList<Node> subtree2 = new ArrayList<>();
		dfs(x, subtree1, false);
		dfs(y, subtree2, false);
		
		int opt = -1;
		IloCplex model = null;
		try {
			model = new IloCplex();
			model.setOut( null ); // avoid cplex's output
			model.setParam(IloCplex.IntParam.Threads, 1); // single thread

			IloNumVar[][] m = new IloNumVar[t1.size()][t2.size()];
			for (int i = 1; i < subtree1.size(); i++) {
				int id1 = subtree1.get(i).id;
				for (int j = 1; j < subtree2.size(); j++) {
					int id2 = subtree2.get(j).id;
					m[id1][id2] = model.boolVar();
				}
			}

			IloNumExpr obj = null;
			for (int i = 1; i < subtree1.size(); i++) {
				int id1 = subtree1.get(i).id;
				for (int j = 1; j < subtree2.size(); j++) {
					int id2 = subtree2.get(j).id;
					obj = sum(model, obj, model.prod(dp[id1][id2], m[id1][id2]));
				}
			}

			model.addMaximize( obj );

			// add constraints: for each path P between the root and a leaf, m(P) must be at most one
			for (Node node: subtree1) {
				if (node.isLeaf() == false) continue;
				// node must be a leaf
				IloNumExpr constraint = null;
				while (node != x) {
					for (int i = 1; i < subtree2.size(); i++) {
						Node match = subtree2.get(i);
						constraint = sum(model, constraint, m[node.id][match.id]);
					}
					node = node.parent;
				}

				if (constraint != null) {
					model.addLe( constraint, 1 );
				}

			}
			for (Node node: subtree2) {
				if (node.isLeaf() == false) continue;
				// node must be a leaf
				IloNumExpr constraint = null;
				while (node != y) {
					for (int i = 1; i < subtree1.size(); i++) {
						Node match = subtree1.get(i);
						constraint = sum(model, constraint, m[match.id][node.id]);
					}
					node = node.parent;
				}

				if (constraint != null) {
					model.addLe( constraint, 1 );
				}
			}

			if (model.solve()) {
				opt = (int)(model.getObjValue() + 0.5) + weight(x.label, null) + weight(null, y.label) - weight(x.label, y.label);
				submapping[x.id][y.id] = new ArrayList<Node[]>();
				for (Node n1: subtree1) {
					for (Node n2: subtree2) {
						if (m[n1.id][n2.id] != null && model.getValue(m[n1.id][n2.id]) > 0.5) {
							submapping[x.id][y.id].add(new Node[] {n1, n2});
						}
					}
				}
			}
		} catch (IloException e) {
			e.printStackTrace();
		} finally {
			if (model != null) {
				model.end();
			}
		}
		return opt;
	}
	
	private IloNumExpr sum(IloCplex model, IloNumExpr ex1, IloNumExpr ex2) throws IloException {
		if (ex1 == null) return ex2;
		if (ex2 == null) return ex1;
		return model.sum(ex1, ex2);
	}

	private int weight(String label1, String label2)
	{
		if (label1 == null) return DELETE_COST;
		if (label2 == null) return INSERT_COST;
		if (label1.equals(label2)) return 0;
		return REPLACE_COST;
	}

	private void dfs(Node r, ArrayList<Node> list, boolean set) 
	{
		list.add(r);
		if (set) {
			r.height = 0;
			r.size = 1;
		}
		for (Node ch: r) {
			dfs(ch, list, set);
			if (set) {
				r.height = Math.max(r.height, ch.height + 1);
				r.size += ch.size;
			}
		}
	}
}
