import ilog.concert.IloException;
import ilog.concert.IloNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;
import java.util.*;

public class IP_Bot {
	
	private static final int REPLACE_COST = 1;
	private static final int DELETE_COST = 1;
	private static final int INSERT_COST = 1;

	private ArrayList<Node> t1;
	private ArrayList<Node> t2;
	private Node r1;
	private Node r2;
	
	private ArrayList<Node[]> mapping;
	

	// compute the Bottom-up Distance by IP formulation due to [Kondo et al.]
	public IP_Bot(Node r1, Node r2)
	{
		this.r1 = r1;
		this.r2 = r2;
		t1 = new ArrayList<>();
		t2 = new ArrayList<>();
		dfs(r1, t1);
		dfs(r2, t2);
	}
	
	public int solve()
	{	
		int W1 = 0;
		for (Node x: t1) {
			W1 += weight(x.label, null);
		}
		int W2 = 0;
		for (Node y: t2) {
			W2 += weight(null, y.label);
		}
		return run(r1, r2) + W1 + W2;
	}
	
	public ArrayList<Node[]> getMapping()
	{
		if (mapping == null) {
			solve();
		}
		return mapping;
	}
	
	private IloNumExpr sum(IloCplex model, IloNumExpr ex1, IloNumExpr ex2) throws IloException
	{
		if (ex1 == null) return ex2;
		if (ex2 == null) return ex1;
		return model.sum(ex1, ex2);
	}

	private int run(Node r1, Node r2)
	{
		int opt = -1;
		IloCplex model = null;
		try {

			model = new IloCplex();
			model.setOut( null ); // avoid cplex's output
			model.setParam(IloCplex.IntParam.Threads, 1); // single thread
			
			IloNumVar[][] m = new IloNumVar[t1.size()][t2.size()];
			IloNumExpr obj = null;
			for (Node x: t1) {
				for (Node y: t2) {
					m[x.id][y.id] = model.boolVar();
					obj = sum(model, obj, model.prod(weight(x.label, y.label) - weight(x.label, null) - weight(null, y.label), m[x.id][y.id]));
				}
			}
			model.addMinimize( obj );


			// add constraints
			// one to one mapping
			for (Node x: t1) {
				IloNumExpr constraint = null;
				for (Node y: t2) {
					constraint = sum(model, constraint, m[x.id][y.id]);
				}
				model.addLe(constraint, 1);
			}
			for (Node y: t2) {
				IloNumExpr constraint = null;
				for (Node x: t1) {
					constraint = sum(model, constraint, m[x.id][y.id]);
				}
				model.addLe(constraint, 1);
			}

			// a.d. relationship
			for (Node des1: t1) {
				for (Node ans1 = des1.parent; ans1 != null; ans1 = ans1.parent) {
					for (Node des2: t2) {
						for (Node ans2: t2) {
							if (isAncestor(des2, ans2) == false) {
								IloNumExpr ex = sum(model, m[ans1.id][ans2.id], m[des1.id][des2.id]);
								model.addLe(ex, 1);
							}
						}
					}
				}
			}
			for (Node des2: t2) {
				for (Node ans2 = des2.parent; ans2 != null; ans2 = ans2.parent) {
					for (Node des1: t1) {
						for (Node ans1: t1) {
							if (isAncestor(des1, ans1) == false) {
								IloNumExpr ex = sum(model, m[ans1.id][ans2.id], m[des1.id][des2.id]);
								model.addLe( ex, 1 );
							}
						}
					}
				}
			}

			for (Node x: t1) {
				for (Node y: t2) {
					for (Node cx: x) {
						IloNumExpr constraint = null;
						for (Node cy: y) {
							constraint = sum(model, constraint, m[cx.id][cy.id]);
						}
						if (constraint != null) {
							model.addLe(m[x.id][y.id], constraint);
						} else {
							// (x, y) cannot be in the mapping
							model.addLe(m[x.id][y.id], 0);
						}
					}
					
					for (Node cy: y) {
						IloNumExpr constraint = null;
						for (Node cx: x) {
							constraint = sum(model, constraint, m[cx.id][cy.id]);
						}
						if (constraint != null) {
							model.addLe(m[x.id][y.id], constraint);
						} else {
							model.addLe(m[x.id][y.id], 0);
						}
					}
				}
			}

			if (model.solve()) {
				opt = (int)(model.getObjValue() - 0.5);
				
				mapping = new ArrayList<>();
				for (Node x: t1) {
					for (Node y: t2) {
						if (model.getValue(m[x.id][y.id]) > 0.5) {
							mapping.add(new Node[] {x, y});
						}
					}
				}
			}

		} catch (IloException e) {
			e.printStackTrace();
		} finally {
			if (model != null) {
				model.end();
			}
		}
		return opt;
	}

	// decide if ans is a proper ancestor of des
	private boolean isAncestor(Node des, Node ans)
	{
		for (Node node = des.parent; node != null; node = node.parent) {
			if (node == ans) {
				return true;
			}
		}
		return false;
	}

	private int weight(String label1, String label2)
	{
		if (label1 == null) return DELETE_COST;
		if (label2 == null) return INSERT_COST;
		if (label1.equals(label2)) return 0;
		return REPLACE_COST;
	}

	private void dfs(Node r, ArrayList<Node> list) 
	{
		list.add(r);
		r.height = 0;
		r.size = 1;
		for (Node ch: r) {
			dfs(ch, list);
			r.height = Math.max(r.height, ch.height + 1);
			r.size += ch.size;
		}
	}
}
